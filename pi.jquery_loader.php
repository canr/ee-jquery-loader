<?php if ( !defined ('BASEPATH')) exit ('No direct script access allowed');

// author: Dennis Bond, Michigan State University (bonddenn@msu.edu)
// February 3, 2010

$plugin_info = array (
	'pi_name'		=> 'jQuery Loader',
	'pi_version'		=> '1.0',
	'pi_author' 		=> 'Dennis Bond',
	'pi_description' 	=> 'Provides the jQuery script tags and document.ready function'
);

class Jquery_loader
{
	// need this because constructors cannot return data
	var $return_data = "";

	function Jquery_loader()
	{
		// get an instance of the ExpressionEngine object
		$this->EE =& get_instance();
		
		// fetch data
		$document_ready_code = $this->EE->TMPL->tagdata;
		$plugins_list = $this->EE->TMPL->fetch_param('plugins');
		$version = $this->EE->TMPL->fetch_param('version', '1.6.2');
		$anr_bar = $this->EE->TMPL->fetch_param('anr_bar');
        $load_jquery = $this->EE->TMPL->fetch_param('load_jquery');
        
		// break the plugins list into an array
		$plugins = explode(",", $plugins_list);
		
		// generate the jquery code
        $jquery_code = "";
        if ($load_jquery!="no")
            $jquery_code .= "<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/{$version}/jquery.min.js\"></script>\n";
        if ($anr_bar=="yes")
            $jquery_code .= "<script type=\"text/javascript\" src=\"http://expeng.anr.msu.edu/anr_bar/network_element.js\"></script>\n";
		if ($plugins_list)
            foreach ($plugins as $plugin)
				$jquery_code .= "<script type=\"text/javascript\" src=\"http://expeng.anr.msu.edu/javascript/".$plugin.".js\"></script>\n";
		$jquery_code .= "<script type=\"text/javascript\">\n"
				."//<!-- hide javascript from validator\n"
				."    $(document).ready(function(){"
				.$document_ready_code
				."    });\n"
				."// end hiding from validator -->\n"
				."</script>";

		$this->return_data = $jquery_code;
	}
}
?>